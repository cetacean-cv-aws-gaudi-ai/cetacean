# Cetacean #



### What is Cetacean? ###

**Cetacean** is a computer vision app that automates whale and dolphin photo-ID by the shape and markings on their tails, dorsal fins, heads, and lateral body views.